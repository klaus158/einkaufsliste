"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import include, re_path
#from einkaufsliste.views import PostenAutocomplete

from . import views
urlpatterns = [
    path('', views.index, name='index'),
    path('add_item', views.add_item, name='add_item'),
    path('neue_liste', views.neue_liste, name='neue_liste'),
    path('delete_eintrag_link/<int:pid>/<str:redirectzurview>', views.delete_eintrag_link, name='delete_eintrag_link'),
    path('sicherheitsabfrage_link/<int:lid>/<str:redirectzurview>', views.sicherheitsabfrage_link, name='sicherheitsabfrage_link'),
    path('listeloeschen', views.listeloeschen, name='listeloeschen'),
    path('rename_liste', views.rename_liste, name='rename_liste'),
    path('item_gekauft/<int:pid>', views.item_gekauft, name='item_gekauft'),
    path('movelist_link/<int:lid>/<str:redirectzurview>', views.movelist_link, name='movelist_link'),
    path('archiv', views.archiv, name='archiv'),
    re_path(r'^posten-autocomplete/$', views.PostenAutocomplete.as_view(), name='posten-autocomplete',)
]