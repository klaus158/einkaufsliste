from django.db import models

# Create your models here.




class liste(models.Model):
    # id (wird von Django erstellt)
    name = models.CharField(max_length=200, default='Neue Einkaufsliste')
    im_archiv = models.BooleanField(default=False)

    def __str__(self):
        archivierttext = "im Archiv"
        if not self.im_archiv:
            archivierttext = "aktive Liste"
        return f"{self.name} ({archivierttext})"

class posten(models.Model):
    # id (wird von Django erstellt)
    name = models.CharField(max_length=200, default='')
    listenid = models.ForeignKey(liste, on_delete=models.CASCADE)
    posten_gekauft = models.BooleanField(default=False)

    def __str__(self):
        gekaufttext = "durchgestrichen"
        if not self.posten_gekauft:
            gekaufttext = "offen auf der Liste"
        return f"{self.name} ({gekaufttext})"

    


