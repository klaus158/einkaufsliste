# Generated by Django 2.2.5 on 2021-05-02 10:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='liste',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='Neue Einkaufsliste', max_length=200)),
                ('im_archiv', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='posten',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=200)),
                ('posten_gekauft', models.BooleanField(default=False)),
                ('listenid', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='einkaufsliste.liste')),
            ],
        ),
    ]
