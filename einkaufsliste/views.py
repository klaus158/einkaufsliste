from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from . import models


from dal import autocomplete

class PostenAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        abfragemenge = models.posten.objects.all()

        if self.q:
            abfragemenge = abfragemenge.filter(name__istartswith=self.q)

        return abfragemenge




def index(request):
    context = {}
    listenQ = models.liste.objects.filter(im_archiv=False)
    #postenQ = models.posten.objects.all()
    context["listenQ"] = listenQ
    #context["postenQ"] = postenQ
    return render(request, "einkaufsliste\index.html", context)

def add_item(request):
    if request.POST:
        neuer_posten = models.posten()
        neuer_posten.name = request.POST["name"]
        neuer_posten.listenid_id = request.POST["lid"]
        neuer_posten.save()
        return redirect("index")

def neue_liste(request):
    liste = models.liste()
    liste.save()
    return redirect("index")


def delete_eintrag_link(request, pid, redirectzurview):
    postenloeschen = get_object_or_404(models.posten, id=pid)
    postenloeschen.delete()
    print( f"delete_eintrag_link: redirectzurview: {redirectzurview}, Typ: ",type(redirectzurview))
    if redirectzurview == "archivview":
        return redirect("archiv") 
    return redirect("index")


def sicherheitsabfrage_link(request, lid, redirectzurview):
    context = {}
    context["delete_liste_id"] = lid
    context["redirectzurview"] = redirectzurview
    context["listenname"] = get_object_or_404(models.liste, id=lid).name
    context["postenliste"] = models.posten.objects.filter(listenid=lid)
    print( "delete_eintrag_link: postenliste: ",context["postenliste"])
    print( f"delete_eintrag_link: redirectzurview: {redirectzurview}, Typ: ",type(redirectzurview))
    print( f"delete_eintrag_link: lid: {lid}, Typ: ",type(lid))
    return render(request, "einkaufsliste\sicherheitsabfrage.html", context)

def listeloeschen(request):
    redirectzurview = "indexview"
    if request.POST:
        delete_liste_id = request.POST["lid"]
        redirectzurview = request.POST["redirectzurview"]
        listeloeschen = get_object_or_404(models.liste, id=delete_liste_id)
        listeloeschen.delete()
    if redirectzurview == "archivview":
        return redirect("archiv") 
    return redirect("index")    
  
def rename_liste(request):
    redirectzurview = "indexview"
    if request.POST:
        lid = request.POST["lid"]
        l_name = request.POST["name"]
        redirectzurview = request.POST["redirectzurview"]
        rename_list = get_object_or_404(models.liste, id=lid)
        rename_list.name=l_name
        rename_list.save()
    if redirectzurview == "archivview":
        return redirect("archiv") 
    return redirect("index")

def item_gekauft(request, pid):
    print(f"item_gekauft / pid: {pid}")
    posten = get_object_or_404(models.posten, id=pid)
    posten.posten_gekauft = not posten.posten_gekauft
    posten.save()
    return redirect("index")

def movelist_link(request, lid, redirectzurview):
    move_list = get_object_or_404(models.liste, id=lid)
    move_list.im_archiv = not move_list.im_archiv
    move_list.save()
    print(f"lid erhalten: {lid} Typ: ", type(lid)) 
    print(f"redirectzurview erhalten: {redirectzurview} Typ: ", type(redirectzurview)) 
    if redirectzurview == "archivview":
        print("Zur Archiv-View")
        return redirect("archiv") 
    return redirect("index")

def archiv(request):
    context = {}
    listenQ = models.liste.objects.filter(im_archiv=True)
    postenQ = models.posten.objects.all()
    for liste in listenQ:
        for posten in liste.posten_set.all():
            posten.posten_gekauft = False
            posten.save()
    context["listenQ"] = listenQ
    context["postenQ"] = postenQ
    return render(request, "einkaufsliste\\archiv.html", context)
